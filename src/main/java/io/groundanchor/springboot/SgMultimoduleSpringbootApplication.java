package io.groundanchor.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SgMultimoduleSpringbootApplication {

  public static void main(String[] args) {
    SpringApplication.run(SgMultimoduleSpringbootApplication.class, args);
  }

}
